
public class KonsolenausgabeA2 {

	public static void main(String[] args) {
		
		String s = "********";
		String Fa = "Fahrenheit";
		String Ce = "Celsius";
		String Tr = "----------------------";
		
		int a = 1;
		int b = 1;
		int c = 2;
		int d = 6;
		int e = 24;
		int f = 120;
		
		int g0 = -20;
		int h0 = -10;
		int i0 = 0;
		int j0 = 20;
		int k0 = 30;
		
		float g1 = -28.8889f;
		float h1 = -23.3333f;
		float i1 = -7.7778f;
		float j1 = -6.6667f;
		float k1 = -1.1111f;
		
		
		//Aufgabe 1
		
		System.out.println("Aufgabe 1\n\n");
		System.out.printf("%5.2s\n", s);
		System.out.printf("%1.1s", s);
		System.out.printf("%7.1s\n", s);
		System.out.printf("%1.1s", s);
		System.out.printf("%7.1s\n", s);
		System.out.printf("%5.2s\n\n", s);
		
		//Aufbage 2
		
		System.out.println("\nAufgabe 2\n\n");				// erste Reihe
		System.out.printf("%-5s" , "0!");
		System.out.printf("%1s" , "=");
		System.out.printf("%-19s" , " ");
		System.out.printf("%1s", " = ");
		System.out.printf("%4d\n" , a);
		
		System.out.printf("%-5s" , "1!");					// zweite Reihe
		System.out.printf("%1s" , "=");
		System.out.printf("%-19s" , "1");
		System.out.printf("%1s", " = ");
		System.out.printf("%4d\n" , b);
		
		System.out.printf("%-5s" , "2!");					// dritte Reihe
		System.out.printf("%1s" , "=");
		System.out.printf("%-19s" , "1 * 2");
		System.out.printf("%1s", " = ");
		System.out.printf("%4d\n" , c);
		
		System.out.printf("%-5s" , "3!");					// vierte Reihe
		System.out.printf("%1s" , "=");
		System.out.printf("%-19s" , "1 * 2 * 3");
		System.out.printf("%1s", " = ");
		System.out.printf("%4d\n" , d);
		
		System.out.printf("%-5s" , "4!");					// f�nfte Reihe
		System.out.printf("%1s" , "=");
		System.out.printf("%-19s" , "1 * 2 * 3 * 4");
		System.out.printf("%1s", " = ");
		System.out.printf("%4d\n" , e);
		
		System.out.printf("%-5s" , "5!");					// sechste Reihe
		System.out.printf("%1s" , "=");
		System.out.printf("%-19s" , "1 * 2 * 3 * 4 * 5");
		System.out.printf("%1s", " = ");
		System.out.printf("%4d\n\n" , f);
		
		//Aufbage 3
		
		System.out.println("\nAufgabe 3\n\n");
		System.out.printf("%-11s|", Fa);
		System.out.printf("%10s\n", Ce);
		
		System.out.printf("%22s\n", Tr);
		
		System.out.printf("%+-11d|", g0);
		System.out.printf("%10.2f\n", g1);
		
		System.out.printf("%+-11d|", h0);
		System.out.printf("%10.2f\n", h1);
		
		System.out.printf("%+-11d|", i0);
		System.out.printf("%10.2f\n", i1);
		
		System.out.printf("%+-11d|", j0);
		System.out.printf("%10.2f\n", j1);
		
		System.out.printf("%+-11d|", k0);
		System.out.printf("%10.2f\n", k1);
		
		
	}

}
