
public class Temperaturtabelle {

	public static void main(String[] args) 
	{
		
		String Fa = "Fahrenheit";
		String Ce = "Celsius";
		String Tr = "----------------------";
		
		int g0 = -20;
		int h0 = -10;
		int i0 = 0;
		int j0 = 20;
		int k0 = 30;
		
		float g1 = -28.8889f;
		float h1 = -23.3333f;
		float i1 = -7.7778f;
		float j1 = -6.6667f;
		float k1 = -1.1111f;
		
		System.out.printf("%-11s|", Fa);
		System.out.printf("%10s\n", Ce);
		
		System.out.printf("%22s\n", Tr);
		
		System.out.printf("%+-11d|", g0);
		System.out.printf("%10.2f\n", g1);
		
		System.out.printf("%+-11d|", h0);
		System.out.printf("%10.2f\n", h1);
		
		System.out.printf("%+-11d|", i0);
		System.out.printf("%10.2f\n", i1);
		
		System.out.printf("%+-11d|", j0);
		System.out.printf("%10.2f\n", j1);
		
		System.out.printf("%+-11d|", k0);
		System.out.printf("%10.2f\n", k1);
		
		}

}
