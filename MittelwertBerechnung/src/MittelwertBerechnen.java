import java.util.Scanner;

public class MittelwertBerechnen 
{

	public static void main(String[] args) 
	{
		double zahl;
		double summe = 0;
		double m;
		
		Scanner myScanner = new Scanner(System.in);
		System.out.println("Dieses Programm berechnet den Mittelwert zweier Zahlen.");
		System.out.print("Wie viele zahlen wollen Sie eingeben?: ");
		int anzahl = myScanner.nextInt();
		
		for(int i = 0; i < anzahl; i++)
		{
			zahl = eingabe(myScanner, "Bitte geben Sie eine Zahl ein: ");
			summe += zahl;
		}
		
		m = mittelwertBerechnung(summe, anzahl);
		
		ausgabe(m);
		
		myScanner.close();
	}
	
	public static double eingabe(Scanner ms, String text ) {
		System.out.print(text);
		double zahl = ms.nextDouble();
		return zahl;
	}
	
	public static double mittelwertBerechnung(double summe, int anzahl) {
		double m = summe / anzahl;
		return m;
	}
	
	public static void ausgabe(double mittelwert) {
		System.out.printf("Der errechnete Mittelwert beider Zahlen ist: %.2f", mittelwert);
	}
}
