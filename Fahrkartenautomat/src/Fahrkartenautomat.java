﻿import java.util.Scanner;

class Fahrkartenautomat {
	public static void main(String[] args) 
	{
		double zuZahlenderBetrag;
		double rueckgabebetrag;
		double[] preis = {2.90, 3.30, 3.60, 1.90, 8.60, 9.00, 9.60, 23.50, 24.30, 24.90};
		String[] bezeichnung = {"Einzelfahrschein Berlin AB", "Einzelfahrschein Berlin BC", "Einzelfahrschein Berlin ABC",
				"Kurzstrecke", "Tageskarte Berlin AB", "Tageskarte Berlin BC", "Tageskarte Berlin ABC", 
				"Kleingruppen-Tageskarte Berlin AB", "Kleingruppen-Tageskarte Berlin BC", "Kleingruppen-Tageskarte Berlin ABC"};

		Scanner tastatur = new Scanner(System.in);
		
		while(true)
		{
			zuZahlenderBetrag = fahrkartenbestellungErfassen(tastatur, preis, bezeichnung);
			rueckgabebetrag = fahrkartenBezahlen(tastatur, zuZahlenderBetrag);
			rueckgeldAusgebe(rueckgabebetrag);
			fahrkartenAusgeben();
		}
		
	}

	// --------------------------- //
	// -----Anfang Methoden------- //
	// --------------------------- //

	// Erfassung des Ticketkaufs
	// -------------------------
	private static double fahrkartenbestellungErfassen(Scanner tastatur, double[] preis, String[] bezeichnung) 
	{
		double ticketPreis = 0;
		double gesamtPreis = 0;
		int anzTickets = 0;
		int wahl = 0;
		boolean falseinput = true;
		boolean invalidVariable = false;

		for(int i = 0; i < bezeichnung.length; i++)
		{
			System.out.printf("[%d] %s: %.2f\n", (i+1), bezeichnung[i], preis[i]);
		}
		
		System.out.print("Welches Ticket wollen sie Kaufen?: ");
		
		
		while(falseinput)
		{
			try 
			{
				wahl = tastatur.nextInt();
			}
			
			//Wenn die Eingabe einen falschen Variablentyp hat, wird der Buffer des "Scanners" geleert.
			//Damit bei der nächsten Zuweisung nicht der Wert aus dem Buffer genommen wird und es erneut zu einem Fehler kommt.
			catch (Exception e)
			{
				tastatur.next();
			}
			finally
			{
				if(wahl > 0 && wahl <= bezeichnung.length)
				{
					ticketPreis = preis[wahl-1];
					falseinput = false;
				}
				else
				{
					System.out.printf("Sie haben eine ungültige Eingabe getätigt bitte versuchen eine Zahl von 1 bis %d ein: ", bezeichnung.length);
				}
			}
		}
		
		
		System.out.print("Anzahl der Tickets: ");
		falseinput = true;
				
		while(falseinput)
		{	
			try 
			{
				anzTickets = tastatur.nextInt();
			}
			
			//Wenn die Eingabe einen falschen Variablentyp hat, wird der Buffer des "Scanners" geleert.
			//Damit bei der nächsten Zuweisung nicht der Wert aus dem Buffer genommen wird und es erneut zu einem Fehler kommt.
			catch (Exception e)
			{
				tastatur.next();
				invalidVariable = true;
			}
			finally
			{
				if(anzTickets > 10 && !invalidVariable)
				{
					System.out.print("\nSie haben zu viele Tickets angegeben es sind max. 10 Tickets zulässig\n"
							+ "Geben Sie eine gültige Anzahl Tickets an: ");
					
					anzTickets = tastatur.nextInt();
					
				}
				else if (anzTickets <= 0 && !invalidVariable)
				{
					System.out.print("\nSie haben entweder null oder einen negativen Betrag an Tickets angegeben.\n"
							+ "Es muss mind. ein Ticket angegeben werden.\n"
							+ "Die Ticketanzahl wird auf 1 gesetzt !\n\n");
					
					anzTickets = 1;
				}
				else if (invalidVariable)
				{
					System.out.print("\nIhr Eingabeformat stimmt nicht.\nBitte geben sie nur natürliche Zahlen ein: ");
					invalidVariable = false;
				}
				else
				{
					falseinput = false;
				}
			}
			
		}

		gesamtPreis = ticketPreis * anzTickets;
		
		return gesamtPreis;
	}

	// Geldeinwurf
	// -----------
	private static double fahrkartenBezahlen(Scanner tastatur, double zuZahlenderBetrag) 
	{
		double eingezahlterGesamtbetrag = 0.0;
		double eingeworfeneMuenze = 0.0;
		double zuZahlen = 0.0;
		boolean invalidinput = true;
		boolean invalidVariable = false;
		double rueckgabe = 0.0;

		while (eingezahlterGesamtbetrag < zuZahlenderBetrag || invalidinput)
		{
			zuZahlen = zuZahlenderBetrag - eingezahlterGesamtbetrag;
			System.out.printf("Noch zu zahlen: %.2f\n", zuZahlen);
			System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
			
			try 
			{
				eingeworfeneMuenze = tastatur.nextDouble();
			}
			catch (Exception e)
			{
				tastatur.next();
				invalidVariable = true;
			}
			finally 
			{
				if(invalidVariable)
				{
					System.out.println("\nBitte benutzen sie Zahlen.\nZur Darstellung von Kommazahlen verwenden Sie bitte ein ','.");
					invalidVariable = false;
				}
				else if (eingeworfeneMuenze == 0.05 || eingeworfeneMuenze == 0.10 || eingeworfeneMuenze == 0.20 || eingeworfeneMuenze == 0.50 || eingeworfeneMuenze == 1.0 || eingeworfeneMuenze == 2.00)
				{
					eingezahlterGesamtbetrag += eingeworfeneMuenze;
					invalidinput = false;
				} 
				else
				{
					System.out.println("Ungültige Eingabe !\nSie können nur mit 5, 10, 20, 50 CENT oder mit 1, 2 EURO bezahlen");
					invalidinput = true;
				}
			}
		}
		
		rueckgabe = eingezahlterGesamtbetrag - zuZahlenderBetrag;

		return rueckgabe;
	}

	// Rückgeldberechnung und -Ausgabe
	// -------------------------------
	private static void rueckgeldAusgebe(double rueckgabebetrag) 
	{
		if (rueckgabebetrag > 0.0) 
		{
			System.out.printf("\nDer Rückgabebetrag in Höhe von %.2f EURO", rueckgabebetrag);
			System.out.println("\nwird in folgenden Münzen ausgezahlt:");

			while (rueckgabebetrag >= 2.0) // 2 EURO-Münzen
			{
				muenzeAusgeben(2, "EURO");
				rueckgabebetrag -= 2.0;
			}
			while (rueckgabebetrag >= 1.0) // 1 EURO-Münzen
			{
				muenzeAusgeben(1, "EURO");
				rueckgabebetrag -= 1.0;
			}
			while (rueckgabebetrag >= 0.5) // 50 CENT-Münzen
			{
				muenzeAusgeben(50, "CENT");
				rueckgabebetrag -= 0.5;
			}
			while (rueckgabebetrag >= 0.2) // 20 CENT-Münzen
			{
				muenzeAusgeben(20, "CENT");
				rueckgabebetrag -= 0.2;
			}
			while (rueckgabebetrag >= 0.1) // 10 CENT-Münzen
			{
				muenzeAusgeben(10, "CENT");
				rueckgabebetrag -= 0.1;
			}
			while (rueckgabebetrag >= 0.05)// 5 CENT-Münzen
			{
				muenzeAusgeben(5, "CENT");
				rueckgabebetrag -= 0.05;
			}
		}

		System.out.println("\n\nFahrschein wird ausgegeben");
		
		for (int i = 0; i < 8; i++)
		{
			System.out.print("=");
			warte(250);
		}
		
		System.out.println("\n\n");
	}

	// Fahrscheinausgabe
	// -----------------
	private static void fahrkartenAusgeben() 
	{
		System.out.println("Vergessen Sie nicht, den Fahrschein\n" + "vor Fahrtantritt entwerten zu lassen!\n"
				+ "Wir wünschen Ihnen eine gute Fahrt.\n\n");
	}

	// Warten
	// ------
	private static void warte(int millisekunde) 
	{
		try 
		{
			Thread.sleep(millisekunde);
		}
		catch (InterruptedException e) 
		{
			e.printStackTrace();
		}
	}

	// Rueckgabeformat
	// ---------------
	private static void muenzeAusgeben(int betrag, String einheit) 
	{
		System.out.printf("%d %s\n", betrag, einheit);
	}
	
}