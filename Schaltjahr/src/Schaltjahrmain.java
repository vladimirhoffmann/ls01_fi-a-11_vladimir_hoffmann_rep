import java.sql.Time;
import java.time.LocalDateTime;
import java.util.Scanner;

public class Schaltjahrmain
{
	public static void main(String[] args) 
	{
		int[] jahre = {1980, 1972, 1720, 1700, 1800, 2000, 1600, 1400};
		int jahr = LocalDateTime.now().getYear();
		
		schaltjahrBest(jahr);
		
		for(int i = 0; i < jahre.length; i++)
		{
			schaltjahrBest(jahre[i]);	
		}
		
		jahr = jahrAbfrage(new Scanner(System.in));
		schaltjahrBest(jahr);
		
	}
	
	public static int jahrAbfrage(Scanner tastatur)
	{
		System.out.print("Geben sie ein Jahr ein: ");
		int jahr = tastatur.nextInt();
		
		return jahr;
	}
	
	public static void schaltjahrBest(int jahr)
	{
		boolean istSchaltjahr;
		
		if (jahr % 4 == 0)
		{
			istSchaltjahr = true;
			
			if(jahr % 100 == 0 && jahr > 1582)
			{
				istSchaltjahr = false;
			}
				
			if(jahr % 400 == 0)
			{
				istSchaltjahr = true;				
			}
		}
		else
		{
			istSchaltjahr = false;
		}
			
		if(istSchaltjahr)
		{
			System.out.printf("Das Jahr %d ist ein Schaltjahr\n", jahr);
		}
		else 
		{
			System.out.printf("Das Jahr %d ist kein Schaltjahr\n", jahr);
		}

	}
}
