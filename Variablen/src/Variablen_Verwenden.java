
public class Variablen_Verwenden 
{

	public static void main(String[] args)
	{
	   
		int i;
		char menuChoice;
		final int lichtGeschw = 299792458;
		short anzMitglieder;
		final double eElementarladung = 1.602 * Math.pow(10, -19);
		boolean zahlungErfolgt;
		
		
		i = 25;
		System.out.println("Zaehler: " + i);
		
		menuChoice = 'C';
		System.out.println("Zeichen: " + menuChoice);
		
		System.out.println("Lichtgeschwindigkeit: " + lichtGeschw + " m/s");
		
		anzMitglieder = 89;
		System.out.println("Zahl der Vereinmitglieder: " + anzMitglieder);
		
		System.out.println("Elektrische Elementarladung: " + eElementarladung);
		
		zahlungErfolgt = true;
		System.out.println("Die Zahlung ist " + zahlungErfolgt);
		
		
		

		
		/* 1. Ein Zaehler soll die Programmdurchlaeufe zaehlen.
        Vereinbaren Sie eine geeignete Variable */
		/* 2. Weisen Sie dem Zaehler den Wert 25 zu
        und geben Sie ihn auf dem Bildschirm aus.*/  
		/* 3. Durch die Eingabe eines Buchstabens soll der Menuepunkt
        	eines Programms ausgewaehlt werden.
        	Vereinbaren Sie eine geeignete Variable */

		/* 4. Weisen Sie dem Buchstaben den Wert 'C' zu
        und geben Sie ihn auf dem Bildschirm aus.*/
		
		/* 5. Fuer eine genaue astronomische Berechnung sind grosse Zahlenwerte
        notwendig.
        Vereinbaren Sie eine geeignete Variable */

		/* 6. Weisen Sie der Zahl den Wert der Lichtgeschwindigkeit zu
        und geben Sie sie auf dem Bildschirm aus.*/

		/* 7. Sieben Personen gruenden einen Verein. Fuer eine Vereinsverwaltung
        soll die Anzahl der Mitglieder erfasst werden.
        Vereinbaren Sie eine geeignete Variable und initialisieren sie
        diese sinnvoll.*/

		/* 8. Geben Sie die Anzahl der Mitglieder auf dem Bildschirm aus.*/

		/* 9. Fuer eine Berechnung wird die elektrische Elementarladung benoetigt.
        Vereinbaren Sie eine geeignete Variable und geben Sie sie auf
        dem Bildschirm aus.*/

		/*10. Ein Buchhaltungsprogramm soll festhalten, ob eine Zahlung erfolgt ist.
        Vereinbaren Sie eine geeignete Variable. */

		/*11. Die Zahlung ist erfolgt.
        Weisen Sie der Variable den entsprechenden Wert zu
        und geben Sie die Variable auf dem Bildschirm aus.*/

	}

}
