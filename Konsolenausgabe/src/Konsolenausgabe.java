
public class Konsolenausgabe {

	public static void main(String[] args) {
		
		String s = "Beispielsatz";	
		String t = "*************";
		float a = 22.4234234f;
		float b = 111.2222f;
		float c = 4.0f;
		float d = 1000000.551f;
		float e = 97.34f;
		
		
		//Aufgabe 1
		System.out.println("Aufgabe 1\n");
		System.out.printf("Dies ist ein \"%s\".\n", s); 	
		System.out.printf("Ein %s ist das.\n", s);
		
		//Aufgabe 2
		System.out.println("\n\nAufgabe 2\n");
		System.out.printf("%7.1s\n", t);
		System.out.printf("%8.3s\n", t);
		System.out.printf("%9.5s\n", t);
		System.out.printf("%10.7s\n", t);
		System.out.printf("%11.9s\n", t);
		System.out.printf("%12.11s\n", t);
		System.out.printf("%13.13s\n", t);
		System.out.printf("%8.3s\n", t);
		System.out.printf("%8.3s\n", t);
		
		//Aufgabe 3
		System.out.println("\n\nAufgabe 3\n");
		System.out.printf("%.2f\n", a);
		System.out.printf("%.2f\n", b);
		System.out.printf("%.2f\n", c);
		System.out.printf("%.2f\n", d);
		System.out.printf("%.2f\n", e);
		
	}

}
