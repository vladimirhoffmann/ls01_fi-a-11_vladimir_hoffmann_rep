import java.util.Scanner;

public class romzahlen2
{
	static boolean run = true;
	static boolean rom = false;
	static boolean dezi = false;
	static String ausgabeString = "";
	
	public static void main(String[] args)
	{
		Scanner scanner = new Scanner(System.in);
		char[] romZahlen;
		
		while(getRunStatus())
		{
			if(!getRunRom() && !getRunDezi())
			{
				menu(scanner);
			}
			
			else if(getRunRom() || getRunDezi())
			{
				if(getRunRom())
				{
					romZahlen = readRomNumber(scanner);
					berechnerRomZahl(scanner, romZahlen);
					
					System.out.println(getAusgabestring());
				}
				else 
				{
					
				}
			}

		}
		
	}
	
	//--------------------------------------------------------------------------------//
	//--Ueberprueft ob bei der Eingabe die maximale Anzahl an Zeichen ueberschritten--//
	//--wird, wandelt die eingebene roemische Zahl in ein Zeichenfeld um und gibt das-//
	//--Zeichenfeld als Rueckgabewert zurueck.----------------------------------------//
	//--------------------------------------------------------------------------------//
	private static char[] readRomNumber(Scanner scanner)
	{
		boolean wrongChar = false;
		boolean error = false;
		boolean run = true;
		char[] romNumb = null;
		
		int countI = 0;
		int countV = 0;
		int countX = 0;
		int countL = 0;
		int countC = 0;
		int countD = 0;
		int countM = 0;
		
		System.out.print("\nGeben Sie eine roemische Zahl ein : ");
		String inputCharArrayString = scanner.next().toUpperCase();
		
		romNumb = new char[inputCharArrayString.length()];
		
		//Prueft ob gueltige Zeichen verwendet wurden und ob mehr als drei aufeinanderfolgende 
		//'I', 'X', 'C', 'M' sowie ob 'V', 'L', 'D' doppelt auftreten. Wenn die
		//Eingabe ungueltig ist soll man die Eingabe wiederholen.
		while(run)
		{
			
			for(int i = 0; i < inputCharArrayString.length(); i++) 
			{
				romNumb[i] = inputCharArrayString.charAt(i);
				
				if(inputCharArrayString.charAt(i) == 'I')
				{
					if(i > 0)
					{
						if (inputCharArrayString.charAt(i-1) == 'I') countI ++;
					}
					
					if(i == 0)	countI ++;
					
					if(countI > 3) error = true;
				}
				
				if(inputCharArrayString.charAt(i) == 'V') 
				{
					countV ++;
					
					if(countV > 1) error = true;
				}
				
				if(inputCharArrayString.charAt(i) == 'X')
				{
					if(i > 0)
					{
						if (inputCharArrayString.charAt(i-1) == 'X') countX ++;
					}
					
					if(i == 0)	countX ++;
					
					if(countX > 3) error = true;

				}
				
				if(inputCharArrayString.charAt(i) == 'L')
				{
					countL ++;
					
					if(countL > 1) error = true;
				}
				
				if(inputCharArrayString.charAt(i) == 'C')
				{
					if(i > 0)
					{
						if (inputCharArrayString.charAt(i-1) == 'C') countC ++;
					}
					
					if(i == 0)	countC ++;
					
					if(countC > 3) error = true;

				}
				
				if(inputCharArrayString.charAt(i) == 'M')
				{
					if(i > 0)
					{
						if (inputCharArrayString.charAt(i-1) == 'M') countM ++;
					}
					
					if(i == 0)	countM ++;
					
					if(countM > 3) error = true;
				}
				
				if(inputCharArrayString.charAt(i) == 'D')
				{
					countD ++;
					
					if(countD > 1) error = true;
				}
				
				if(inputCharArrayString.charAt(i) != 'I' && inputCharArrayString.charAt(i) != 'V' && inputCharArrayString.charAt(i) != 'X' && inputCharArrayString.charAt(i) != 'E'
				&& inputCharArrayString.charAt(i) != 'L' && inputCharArrayString.charAt(i) != 'C' && inputCharArrayString.charAt(i) != 'M' && inputCharArrayString.charAt(i) != 'D')
				{
					wrongChar = true;
				}
			}
			
			if (!wrongChar && !error)
			{
				run = false;
			}
			
			if(wrongChar || error)
			{
				System.out.print("Ungueltige Eingabe !\n\n"
						+ "Roemische Zahlen werden aus den Zeichen\n"
						+ "'I', 'V', 'X', 'L', 'C', 'D' und 'M' gebildet.\n"
						+ "'I', 'X', 'C' und 'M' koennen drei mal in Folge verwndet werden.\n"
						+ "'V', 'L' und D koennen nur einmalig verwendet werden."
						+ "\n\nGeben Sie eine roemische Zahl ein: ");
				inputCharArrayString = scanner.next().toUpperCase();
				
				romNumb = new char[inputCharArrayString.length()];
				
				countI = 0;
				countV = 0;
				countX = 0;
				countL = 0;
				countC = 0;
				countD = 0;
				countM = 0;
				
				run = true;
				error = false;
				wrongChar = false;
			}
			
		}
		
		return romNumb;
	} 
	
	//-------------------------------------------------------------------------------------------------------------------------------//
	//--Wandelt die eingegebene roemische Zahl in eine Dezimalzahl um und ueberprueft ob die Zahl eine gueltige roemische Zahl ist.--//
	//-------------------------------------------------------------------------------------------------------------------------------//
	private static void berechnerRomZahl(Scanner scanner,char[] romZahl)
	{
		String romZahlString = new String(romZahl);
		char[] zahlChar = romZahl;
		int[] romZahlWert = {1, 5, 10, 50, 100, 500, 1000};
		int decimal = 0;
		
		//Ueberprueft ob die Reihenfolge der Zeichen in dem Feld korrekt ist und rechnet die
		//roemische Zahl in eine Dezimalzahl um
		for(int i = 0; i < zahlChar.length; i++)
		{
			
			switch (zahlChar[i])
			{
				case 'I':
					if(zahlChar[0] == 'I')
					{	
						if(i != zahlChar.length-1 && zahlChar[i+1] != 'I')
						{
								if ((zahlChar.length == 2) && (zahlChar[i+1] == 'V' || zahlChar[i+1] == 'X')) 
								{
									if (zahlChar[i+1] == 'V')
									{
										decimal = decimal + (romZahlWert[1] - romZahlWert[0]);
									}
									
									if (zahlChar[i+1] == 'X')
									{
										decimal = decimal + (romZahlWert[2] - romZahlWert[0]);
									}	
								}
								else 
								{
									System.out.println("Ungueltige Eingabe es darf nur ein 'I' vor einem 'X' oder 'V' stehen");
									
									zahlChar = readRomNumber(scanner); 
									romZahlString = new String(zahlChar);
									
									decimal = 0;
									i = -1;
								}
							
						}
						else 
						{
							decimal += romZahlWert[0];
						}
					}
					
					else 
					{
						if (i != zahlChar.length-1) 
						{
							if((zahlChar[i+1] == 'V' || zahlChar[i+1] == 'X') && (zahlChar.length > 2))
							{
								if (zahlChar[i+1] == 'V')
								{
									decimal = decimal + (romZahlWert[1] - romZahlWert[0]);
								}
								
								else
								{
									if(zahlChar[i-1] != 'V')
									{
										decimal = decimal + (romZahlWert[2] - romZahlWert[0]);
									}
									else 
									{
										System.out.println("Ungueltige Eingabe vor \"IX\" darf kein \"V\" stehen");
											
										zahlChar = readRomNumber(scanner); 
										romZahlString = new String(zahlChar);
										
										decimal = 0;
										i = -1;
									}
								}	
							}
							
							else 
							{
								decimal += romZahlWert[0];
							}
						}
						else 
						{
							decimal += romZahlWert[0];
						}
					}
					break;
				
				case 'V':
					if(zahlChar.length > 1)
					{
						if(i != zahlChar.length-1)
						{	
							if(zahlChar[i+1] == 'X' || zahlChar[i+1] == 'L'  || zahlChar[i+1] == 'C'  || zahlChar[i+1] == 'D'  || zahlChar[i+1] == 'M')
							{
								System.out.println("Ungueltige Eingabe 'V' darf nicht vor 'X', 'L', 'C', 'D' oder 'M' stehen");
								
								zahlChar = readRomNumber(scanner); 
								romZahlString = new String(zahlChar);
								
								decimal = 0;
								i = -1;
							}
							
							else 
							{
								if(i > 1)
								{
									if(zahlChar[i+1] == 'I' && zahlChar[i-1] == 'I')
									{
										System.out.println("Ungueltige Eingabe ein 'I' darf nicht nach einem \"IV\" stehen");
										
										zahlChar = readRomNumber(scanner); 
										romZahlString = new String(zahlChar);
										
										decimal = 0;
										i = -1;
									}
									else 
									{		
										decimal += romZahlWert[1];
									}
								}
								else 
								{		
									decimal += romZahlWert[1];
								}
							}
						}
                        else 
                        {
                            if(zahlChar[i-1] != 'I')
                            {
                                decimal += romZahlWert[1];
                            }
                        }
					}
					else 
					{
						decimal += romZahlWert[1];
					}
					break;
					
				case 'X':
					if(i > 0)
					{
						if (i != zahlChar.length-1)
						{
							if(zahlChar[i-1] == 'X' && zahlChar[i+1] == 'L')
							{
								System.out.println("Ungueltige Eingabe, es darf nur ein \"X\" vor dem \"L\" stehen");
								
								zahlChar = readRomNumber(scanner); 
								romZahlString = new String(zahlChar);
								
								decimal = 0;
								i = -1;
							}
						}
						
						if(zahlChar[i-1] == 'L')
						{
							if (i > 1)
							{
								if (zahlChar[i-2] == 'X')
								{
									System.out.println("Ungueltige Eingabe auf ein \"XL\" daft kein \"X\" folgen");
									
									zahlChar = readRomNumber(scanner); 
									romZahlString = new String(zahlChar);
									
									decimal = 0;
									i = -1;
								}
							}
						}
					}
					
					if(zahlChar.length > 1)
					{	
						if(i != zahlChar.length-1)
						{	
							if(zahlChar[i+1] == 'D'  || zahlChar[i+1] == 'M')
							{
								System.out.println("Ungueltige Eingabe 'X' darf nicht vor einem 'D' oder M stehen");
								
								zahlChar = readRomNumber(scanner);
								romZahlString = new String(zahlChar);
								
								decimal = 0;
								i = -1;
							}
							
							else 
							{
								if ((i != zahlChar.length-1 && zahlChar.length >= 2)) 
								{
									if (zahlChar[i+1] == 'L')
									{
										decimal = decimal + (romZahlWert[3] - romZahlWert[2]);
									}
									
									if (zahlChar[i+1] == 'C')
									{
										decimal = decimal + (romZahlWert[4] - romZahlWert[2]);
									}
									
									if (zahlChar[i+1] != 'L' && zahlChar[i+1] != 'C')
									{
										decimal += romZahlWert[2];
									}
								}
								
								else
								{
									decimal += romZahlWert[2];
								}
							
							}
						}
						
						else
						{
							if(zahlChar[i-1] != 'I')
							{
								decimal += romZahlWert[2];
							}
							
						}
					}
					
					else if(zahlChar[0] == 'X')
					{
						decimal += romZahlWert[2];
					}
					break;
				
				case 'L':
					if(i > 0)
					{
						if(zahlChar[i-1] == 'C' || zahlChar[i-1] == 'D' || zahlChar[i-1] == 'M')
						{
							decimal += romZahlWert[3];
						}
						else 
						{
							if(zahlChar[i-1] == 'X') 
							{
								
							}
							else 
							{
								System.out.println("Ungueltige Eingabe ein 'L' oder ein 'M' darf nicht vor einem 'D' stehen");
								
								zahlChar = readRomNumber(scanner); 
								romZahlString = new String(zahlChar);
								
								decimal = 0;
								i = -1;
							}
						}
					}
					
					if(i == 0)
					{
						decimal += romZahlWert[3];
					}
					break;
					
				case 'C':
					if(i > 0)
					{
						if(zahlChar[i-1] == 'D' || zahlChar[i-1] == 'M' || zahlChar[i-1] == 'C')
						{
							if(i != zahlChar.length-1)
							{
								if(zahlChar[i+1] == 'D' || zahlChar[i+1] == 'M')
								{
									if(zahlChar[i+1] == 'M')
									{
										decimal = decimal + (romZahlWert[6] - romZahlWert[4]);
									}
									else 
									{
										decimal = decimal + (romZahlWert[5] - romZahlWert[4]);
									}
									
								}
								else 
								{
									decimal += romZahlWert[4];
								}
							}
							else 
							{
								decimal += romZahlWert[4];
							}
						}
						
						else 
						{
							if(zahlChar[i-1] == 'X') 
							{
								if(i != zahlChar.length-1 && zahlChar[i+1] == 'L')
								{
									System.out.println("Ungueltige Eingabe ein 'L' darf nicht nach einem \"XC\" stehen");
									
									zahlChar = readRomNumber(scanner); 
									romZahlString = new String(zahlChar);
									
									decimal = 0;
									i = -1;
								}
								
								else if(i != zahlChar.length-1 && zahlChar[i+1] == 'X')
								{
									System.out.println("Ungueltige Eingabe ein 'X' darf nicht nach einem \"XC\" stehen");
									
									zahlChar = readRomNumber(scanner); 
									romZahlString = new String(zahlChar);
									
									decimal = 0;
									i = -1;
								}
							}
							else 
							{
								
								System.out.println("Ungueltige Eingabe ein 'L' darf nicht vor einem 'C' stehen");
								
								zahlChar = readRomNumber(scanner); 
								romZahlString = new String(zahlChar);
								
								decimal = 0;
								i = -1;
							}
						}
					}
					
					if(i == 0)
					{
						if(i != zahlChar.length-1)
						{
							if(zahlChar[i+1] == 'D' || zahlChar[i+1] == 'M')
							{
								if (zahlChar[i+1] == 'D')
								{
									decimal = romZahlWert[5] - romZahlWert[4];
								}
								else 
								{
									decimal = romZahlWert[6] - romZahlWert[4];
								}
							}
							else 
							{
								decimal += romZahlWert[4];
							}
						}
						else 
						{
							decimal += romZahlWert[4];
						}
						
					}
					break;
					
				case 'D':
					if(i > 0)
					{
						if(zahlChar[i-1] == 'M' || zahlChar[i-1] == 'C')
						{
							if (zahlChar.length-1 != i && zahlChar[i-1] == 'C')
							{
								if(zahlChar[i+1] == 'C')
								{
									System.out.println("Ungueltige Eingabe ein 'C' darf nicht nach \"CD\" stehen");
									
									zahlChar = readRomNumber(scanner); 
									romZahlString = new String(zahlChar);
									
									decimal = 0;
									i = -1;
								}
							}
							else 
							{
								decimal += romZahlWert[5];
							}
						}
						else 
						{
							if(zahlChar[i-1] == 'X') 
							{
								
							}
							else 
							{
								System.out.println("Ungueltige Eingabe ein 'L' darf nicht vor einem 'D' stehen");
								
								zahlChar = readRomNumber(scanner); 
								romZahlString = new String(zahlChar);
								
								decimal = 0;
								i = -1;
							}
						}
					}
					if(i == 0)
					{	
						decimal += romZahlWert[5];
					}
					break;
					
				case 'M':
					if(i > 0)
					{
						if(zahlChar[i-1] == 'M')
						{
							decimal += romZahlWert[6];
						}
						
						else 
						{
							if(zahlChar[i-1] == 'C' && i != zahlChar.length-1) 
							{
								if(zahlChar[i+1] == 'C')
								{
									System.out.println("Ungueltige Eingabe ein 'C' darf nicht nach \"CM\" stehen");
									
									zahlChar = readRomNumber(scanner); 
									romZahlString = new String(zahlChar);
									
									decimal = 0;
									i = -1;
								}
								
								else if(zahlChar[i+1] == 'D')
								{
									System.out.println("Ungueltige Eingabe ein 'D' darf nicht nach \"CM\" stehen");
									
									zahlChar = readRomNumber(scanner); 
									romZahlString = new String(zahlChar);
									
									decimal = 0;
									i = -1;
								}
							}
							else 
							{
								System.out.println("Ungueltige Eingabe ein 'D' oder ein 'L' darf nicht vor einem 'M' stehen");
								
								zahlChar = readRomNumber(scanner); 
								romZahlString = new String(zahlChar);
								
								decimal = 0;
								i = -1;
							}
						}
					}
					
					if(i == 0)
					{
						decimal += romZahlWert[6];
					}
					break;
					
				case 'E':
					try 
					{	
						scanner.close();
						System.out.println("\nDas Programm wird beendet.\nBis zum naechsten Mal !");
						Thread.sleep(3000);
						setRunStatus(false);
					} 
					catch (Exception e) 
					{
						// TODO: handle exception
					}
					break;
					
				default:
					break;
			}
		}

		if(zahlChar[0] == 'E')
		{
			setAusgabeString("");
		}
		else 
		{
			setAusgabeString("Die Roemische Zahl " + romZahlString + " entspricht der Dezimalzahl " + decimal);
		}
	}
	
	
	//Menue zur Auswahl der Umwandlungsform DEZI -> ROM oder ROM -> DEZI
	private static void menu(Scanner scanner)
	{
		char wahl;
		boolean error = true;
		
		System.out.print("Druecken sie die Taste\n[r]/[R] wenn Sie roemische Zahlen in Dazimalzahlen umrechen wollen,"
				+ "\n[d]/[D] wenn Sie Dazimalzahlen in roemische Zahlen umrechen wollen und\n"
				+ "[e]/[E] wenn Sie das Programm beenden m�chten: ");
		wahl = scanner.next().toUpperCase().charAt(0);
		
		while(error)
		{
			if(wahl == 'R' || wahl == 'D' || wahl == 'E')
			{
				if (wahl == 'R')
				{
					setRunDezi(false);
					setRunRom(true);
					
					error = false;
				}
				else 
				{
					if (wahl == 'D')
					{
						setRunRom(false);
						setRunDezi(true);
						
						error = false;
					}
					
					else 
					{
						error = false;
						
						setRunRom(false);
						setRunDezi(false);
						
						try 
						{
							scanner.close();
							System.out.println("\nDas Programm wird beendet.\nBis zum naechsten Mal !");
							Thread.sleep(3000);
							setRunStatus(false);
						} 
						catch (Exception e) 
						{
							// TODO: handle exception
						}
					}
				}
			}
			else 
			{
				error = true;
				System.out.print("Ungueltige eingabe bitte versuchen sie es erneut: ");
				wahl = scanner.next().toUpperCase().charAt(0);
			}
		}
	}
	
	//Setter und Getter
	private static void setRunStatus(boolean bool)
	{
		run = bool;
	}
	
	private static boolean getRunStatus()
	{
		return run;
	}
	
	private static void setRunRom(boolean bool)
	{
		rom = bool;
	}
	
	private static boolean getRunRom()
	{
		return rom;
	}
	
	private static void setRunDezi(boolean bool)
	{
		dezi = bool;
	}
	
	private static boolean getRunDezi()
	{
		return dezi;
	}
	
	private static void setAusgabeString(String ausgabe)
	{
		ausgabeString = ausgabe;
	}
	
	private static String getAusgabestring()
	{
		return ausgabeString;
	}
		
}
