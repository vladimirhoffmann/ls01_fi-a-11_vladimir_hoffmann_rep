import java.util.Scanner;

public class Roemischezahlen 
{

	public static void main(String[] args) 
	{
		Scanner scanner = new Scanner(System.in);
		char romZahl = ' ';
		boolean run = true;
		
		
		System.out.print("Geben Sie einen Buchstaben ein der einer Römischenzahl entspricht: ");

		while(run)
		{
			romZahl = scanner.next().toUpperCase().charAt(0);
			
			switch(romZahl)
			{
				case 'I':
					System.out.printf("%s hat eine Wertigkeit von 1", romZahl);
					run = false;
					break;
				
				case 'V':
					System.out.printf("%s hat eine Wertigkeit von 5", romZahl);
					run = false;
					break;
					
				case 'X':
					System.out.printf("%s hat eine Wertigkeit von 10", romZahl);
					run = false;
					break;
					
				case 'L':
					System.out.printf("%s hat eine Wertigkeit von 50", romZahl);
					run = false;
					break;	
					
				case 'C':
					System.out.printf("%s hat eine Wertigkeit von 100", romZahl);
					run = false;
					break;
					
				case 'D':
					System.out.printf("%s hat eine Wertigkeit von 500", romZahl);
					run = false;
					break;
					
				case 'M':
					System.out.printf("%s hat eine Wertigkeit von 1000", romZahl);
					run = false;
					break;
					
				default:
					System.out.print("Üngultige eingabe versuchen Sie es nochmal:");
					
					run = true;
					break;
			}
		}

	}

}

